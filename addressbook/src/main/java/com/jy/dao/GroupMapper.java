package com.jy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.jy.domain.Group;

public interface GroupMapper {
	
	public List<Group> selectGroups(Group group);
	
    int deleteByPrimaryKey(Integer gId);

    int insertSelective(Group record);

    Group selectByPrimaryKey(Integer gId);

    int updateByPrimaryKeySelective(Group record);

	
}
