package com.jy.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {
	
//	@RequestMapping(value="/login")
//	public String login(String userName,String password,
//			HttpSession session){
//		if("admin".equals(userName) && "admin".equals(password)){
//			
//		}
//		return "redirect:/groups";
//	}
	
	@ResponseBody
	@RequestMapping(value="/login")
	public Map<String,Object> validate(String userName,String passWord,HttpSession session){
		Map<String,Object> retMap=new HashMap();
		if("admin".equals(userName) && "admin".equals(passWord)){
			session.setAttribute("user", userName);
			retMap.put("RET_CODE", "success");
		}else{
			retMap.put("RET_CODE", "error");
			retMap.put("RET_MSG", "用户名密码错误");
		}
		
		return retMap;
	}
}


